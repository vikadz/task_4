// 3. Resolve, Reject

const f3 = async (delay, timeout) => {
    try {
        const result = await new Promise(function (resolve, reject) {
            setTimeout(() => {
                if (delay < 5000) {
                    setTimeout(function () {
                        resolve(console.log('Promise resolved'));
                    }, delay);
                } else {
                    reject('Promise rejected');
                };
            }, timeout);
        });
        return (result);
    } catch (error) {
        return console.log(error);
    }
}

async function go3() {
    await f3('Hello', 5000, 5000);
    await f3('Hi', 3000, 3000);
    await f3('Привет', 6000, 1000)
}

go3()







/////////////////////////////////////////////////
// const f3 = async (delay, timeout) => {  
//         try {
//         const result = await new Promise(function (resolve, reject) {
//             setTimeout(() => {
//                 if (delay < 5000) {
//                     setTimeout(function () {
//                         resolve(console.log('Promise resolved'));
//                     }, delay);
//                 } else {
//                     reject('Promise rejected');
//                 };
//             }, timeout);
//         });
//         return (result);
//     } catch (error) {
//         return console.log(error);
//     }
// }

//////////////////////////////////////
// f3(7000, 1000)
//     .then(() => f3(3000, 5000))
//     .then(() => f3(7000, 6000))



//////////////////////////////////////
//
// function f3(delay) {
//     new Promise(function (resolve, reject) {

//         if (delay < 5000) {
//             setTimeout(function () {
//                 resolve('Promise resolved')
//             }, delay)
//         } else {
//             reject('Promise rejected')
//         };
//     }).then(
//         result => console.log(result),
//         error => console.log(error)
//     )
// }


// function go() {

//     setTimeout(() => {
//         f3(4500)
//     }, 1000);

//     setTimeout(() => {
//         f3(6000)
//     }, 6000);

//     setTimeout(() => {
//         f3(3000)
//     }, 12000);
// };

// go();
