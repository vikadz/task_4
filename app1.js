// 1. CallBack

function greeting(name) {
    let hi = ['Hello', 'ألسّلام عليكم', 'Shlomo', 'Вітаю', 'Shalom', '你好', 'Hallo'];

    hi.forEach(function (hello) {
        console.log(`${hello}, ${name} !`);
    });
}

function yourName(callback) {
    const name = 'Garry Potter';
    callback(name);
}

yourName(greeting);