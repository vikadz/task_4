// 4.

const msg = "Hello";

async function f4(msg, delay, timeout) {

    let promise = new Promise((resolve) => {
        setTimeout(() => {
            setTimeout(() => resolve(`Message is: ${msg}. Delay equal: ${delay}`), delay)
        }, timeout);

    });

    let result = await promise;
    console.log(result)
}

async function go4() {
    await f4('Hello', 5000, 5000)
    await f4('Hi', 3000, 3000)
    await f4('Привет', 6000, 1000)
};

go4();





// f4('Hello', 5000, 5000)
//     .then(() => f4('Hi', 3000, 3000))
//     .then(() => f4('Привет', 6000, 1000))



////////////////////////////////////////
//

// const msg = "Hello";
    
// async function f(msg, delay) {

//     let promise = new Promise((resolve) => {
//         setTimeout(() => resolve(`Message is: ${msg}. Delay equal: ${delay}`), delay)
//     });

//     let result = await promise;
//     console.log(result)
// }

// async function go() {

//     await f(msg, 5000)
//         .then(result => result)

//     await f(msg, 3000)
//         .then(result => result)

//     await f(msg, 1000)
//         .then(result => result)

// }

// go();