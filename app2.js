// 2. sendMessageWithDelay


async function sendMessageWithDelay(msg, delay, timeout) {
    setTimeout(() => {
        setTimeout(() => {
            console.log(`Message is: ${msg}. Delay equal: ${delay}`)
        }, delay);
    }, timeout);
};

async function f2() {
    await sendMessageWithDelay('Hello', 5000, 5000);
    await sendMessageWithDelay('Hi', 3000, 3000);
    await sendMessageWithDelay('Привет', 6000, 1000)
}

f2()


/////////////////////////////////////
// async function sendMessageWithDelay(msg, delay, timeout) {
//     setTimeout(() => {
//         setTimeout(() => {
//             console.log(`Message is: ${msg}. Delay equal: ${delay}`)
//         }, delay);  
//     }, timeout);
// };

// sendMessageWithDelay('Hello', 5000, 5000)
//     .then(() => sendMessageWithDelay('Hi', 3000, 3000))
//     .then(() => sendMessageWithDelay('Привет', 6000, 1000))





///////////////////////////////////

// 

// function sendMessageWithDelay(msg, delay) {

//     setTimeout(() => {
//         console.log(`Message is: ${msg}. Delay equal: ${delay}`)
//     }, delay);

// };

// function f2() {

//     setTimeout(() => {
//         sendMessageWithDelay('5 sec', 5000)
//     }, 9000);
//     setTimeout(() => {
//         sendMessageWithDelay('3 sec', 3000)
//     }, 4000);
//     setTimeout(() => {
//         sendMessageWithDelay('1 sec', 1000)
//     }, 1000);

// }
// f2();